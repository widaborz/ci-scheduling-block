# Campo Imperatore Scheduling block backend API
**********

The project provides API to manage the scheduling block of  Campo Imperatore Observatory

To run the server: 

```
cd server
node index.js
```

Check the URL: 

```
http://localhost:3000
```

To retrieve obdata
```
http://localhost:3000/obdata
```

To retrieve oblist
```
http://localhost:3000/oblist
```

To retrieve obdata by ID
```
http://localhost:3000/obdata/242
```

To retrieve obdata by OBNumber
```
http://localhost:3000/obdata/OBnumber/242
```

To retrieve oblist by ID
```
http://localhost:3000/oblist/1
```

To create obdata

```
curl -i -X POST -H 'Accept: application/json' \
    -H 'Content-type: application/json' http://localhost:3000/obdata \
    --data '{"ID":12434,"OBnumber":27,"ins_date":"2020-12-23T11:29:33","Note":null,"_RA":"185.4603","_DEC":"4.4817","Epoch":"2000.000","Exp_type":0,"Exp_time":"60.0","Repeats":3,"Binning":1,"Guider":1,"Filter":4,"Object":"2020jfo","Dithering":"0.0","Focus":0,"Priority":3,"Alt_limit":30,"Status":3,"Activation":0,"Xstart":null,"Ystart":null,"Xstop":null,"Ystop":null,"StartTime":null,"StopTime":null,"Done":3,"Sequence":1,"CCD_overhead":23.100000381469727}'
```

To create oblist

```
curl -i -X POST -H 'Accept: application/json' \
    -H 'Content-type: application/json' http://localhost:3000/oblist \
    --data '{"ID":100123,"ins_date":"2020-11-07T08:43:06","OBnumber":100123,"OBname":"Dome_Flat","Program":"Calibrazioni","Observer":"Robotic Observer","OBpriority":0,"OBstatus":0,"OBactivation":0,"OBstartTime":"2019-12-31T23:00:00","OBstopTime":"2099-12-31T23:00:00","TotalExeTime":0,"Nsequence":1,"Ngroup":null}
```

To update obdata

```
curl -i -X PUT -H 'Accept: application/json' \
    -H 'Content-type: application/json' http://localhost:3000/obdata/12434 \
    --data '{"OBnumber":28,"ins_date":"2020-12-23T11:29:33","Note":null,"_RA":"185.4603","_DEC":"4.4817","Epoch":"2000.000","Exp_type":0,"Exp_time":"60.0","Repeats":3,"Binning":1,"Guider":1,"Filter":4,"Object":"2020jfo","Dithering":"0.0","Focus":0,"Priority":3,"Alt_limit":30,"Status":3,"Activation":0,"Xstart":null,"Ystart":null,"Xstop":null,"Ystop":null,"StartTime":null,"StopTime":null,"Done":3,"Sequence":1,"CCD_overhead":23.100000381469727}'
```

To update oblist

```
curl -i -X PUT -H 'Accept: application/json' \
    -H 'Content-type: application/json' http://localhost:3000/oblist/100123 \
    --data '{"ins_date":"2020-11-07T08:43:06","OBnumber":100123,"OBname":"Dome_Flat","Program":"Calibrazioni","Observer":"Matteo Canzari","OBpriority":0,"OBstatus":0,"OBactivation":0,"OBstartTime":"2019-12-31T23:00:00","OBstopTime":"2099-12-31T23:00:00","TotalExeTime":0,"Nsequence":1,"Ngroup":null}'
```

To delete obdata

```
curl -i -X DELETE -H 'Accept: application/json' \
    -H 'Content-type: application/json' http://localhost:3000/obdata/12434
```

To delete oblist

```
curl -i -X DELETE -H 'Accept: application/json' \
    -H 'Content-type: application/json' http://localhost:3000/oblist/100123
```