const db = require('./db');
const helper = require('../helper');
const config = require('../config');

async function getMultiple(page=1){
    console.log("get oblist");
    const offset = helper.getOffset(page, config.listPerPage);
    const rows = await db.query("SELECT * FROM oblist ORDER by ID desc LIMIT ?, ?", 
                                    [offset, config.listPerPage]); 
    
    const data = helper.emptyOrRows(rows); 
    const meta = {page};

    return {
        data,
        meta 
    } 
}

async function getByID(id) {
    console.log("get oblist by id: ", id)
    const rows = await db.query("SELECT * FROM oblist WHERE ID = ?", 
                                [id]); 
    return helper.emptyOrRows(rows);
}

async function create(oblist) {
    //TODO: create function for datetiem values: ins_date, OBstartTime, OBstopTime

    const rows = await db.query("SELECT max(OBNumber) + 1 as OBNumber, max(ID) + 1 as ID FROM schmidtob.oblist"); 

    result = await db.query(`INSERT INTO oblist
                            (ID, ins_date, OBnumber, OBname, Program, Observer, OBpriority, OBstatus, 
                                OBactivation, OBstartTime, OBstopTime, TotalExeTime, Nsequence, Ngroup)
                            VALUES
                            (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`, 
                            [rows[0].ID, oblist.ins_date, rows[0].OBNumber, oblist.OBname, oblist.Program, 
                            oblist.Observer, oblist.OBpriority, oblist.OBstatus, oblist.OBactivation, 
                            oblist.OBstartTime, oblist.OBstopTime, oblist.TotalExeTime, oblist.Nsequence, oblist.Ngroup])

    let message = "No row(s) affected"; 
    
    if(result.affectedRows)
    {
        message = "Oblist created"; 
    }

    return {message}; 
}

async function update(id, oblist) {
    result = await db.query(`UPDATE oblist
                            SET
                            ins_date = ?, OBnumber = ?, OBname = ?, Program = ?, Observer = ?,
                            OBpriority = ?, OBstatus = ?, OBactivation = ?, OBstartTime = ?,
                            OBstopTime = ?, TotalExeTime = ?, Nsequence = ?, Ngroup = ?
                            WHERE ID = ?`, 
                            [oblist.ins_date, oblist.OBnumber, oblist.OBname, oblist.Program, 
                            oblist.Observer, oblist.OBpriority, oblist.OBstatus, oblist.OBactivation, 
                            oblist.OBstartTime, oblist.OBstopTime, oblist.TotalExeTime, oblist.Nsequence, 
                            oblist.Ngroup, id])

    let message = "No row(s) affected"; 
    if(result.affectedRows)
    {
        message = result.affectedRows + " rows(s) affected"; 
    }

    return {message}; 
}

async function remove(id) {
    result = await db.query(`DELETE FROM oblist
                            WHERE ID = ?`, 
                            [id])

    let message = "No row(s) deleted"; 

    if(result.affectedRows){
        message = result.affectedRows + " row(s) affected";
    }

    return {message}; 
}

module.exports =  {
    getMultiple, 
    getByID, 
    create, 
    update, 
    remove
}