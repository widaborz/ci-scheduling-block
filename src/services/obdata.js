const db = require('./db');
const helper = require('../helper');
const config = require('../config');

async function getMultiple(page = 1){
  console.log("get obdata "); 
  const offset = helper.getOffset(page, config.listPerPage);
  const rows = await db.query(
    `SELECT * FROM schmidtob.obdata LIMIT ?,?`, 
    [offset, config.listPerPage]
  );
  const data = helper.emptyOrRows(rows);
  const meta = {page};

  return {
    data,
    meta
  }
}

async function getFromId(id){
  console.log("get obdata by id: ", id)
  const rows = await db.query(
    `SELECT * FROM schmidtob.obdata WHERE ID = ?`, 
    [id]
  );
  const data = helper.emptyOrRows(rows);

  return {
    data
  }
}

async function getFromOBNumber(OBnumber){
  console.log("get obdata by OBnumber: ", OBnumber)
  const rows = await db.query(
    `SELECT * FROM schmidtob.obdata WHERE OBnumber = ?`, 
    [OBnumber]
  );
  const data = helper.emptyOrRows(rows);

  return {
    data
  }
}

async function create(obdata){
  //TODO: autoincremente ID
  //TODO: generate obdata.ins_date
  const result = await db.query(
    `INSERT INTO obdata
    (OBnumber, Note, _RA, _DEC, Epoch, Exp_type, Exp_time, Repeats, Binning, 
    Guider, Filter, Object, Dithering, Focus, Priority, Alt_limit, Status, Activation, 
    Xstart, Ystart, Xstop, Ystop, StartTime, StopTime, Done, Sequence, CCD_overhead)
    VALUES
    (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);
    `, 
    [
      obdata.OBnumber, obdata.Note, obdata._RA, obdata._DEC, obdata.Epoch, 
      obdata.Exp_type, obdata.Exp_time, obdata.Repeats, obdata.Binning, obdata.Guider, obdata.Filter, 
      obdata.Object, obdata.Dithering, obdata.Focus, obdata.Priority, obdata.Alt_limit, obdata.Status, 
      obdata.Activation, obdata.Xstart, obdata.Ystart, obdata.Xstop, obdata.Ystop, obdata.StartTime, 
      obdata.StopTime, obdata.Done, obdata.Sequence, obdata.CCD_overhead
    ]
  );

  let message = 'Error in creating obdata';

  if (result.affectedRows) {
    message = 'Obdata created successfully';
  }

  return {message};
}

async function update(id, obdata){
  const result = await db.query(
    `UPDATE obdata
    SET
    OBnumber = ?, ins_date = ?, Note = ?, _RA = ?, _DEC = ?, Epoch = ?, Exp_type = ?, Exp_time = ?,Repeats = ?,
    Binning = ?,Guider = ?, Filter = ?, Object = ?, Dithering = ?, Focus = ?, 
    Priority = ?, Alt_limit = ?, Status = ?, Activation = ?, Xstart = ?, Ystart = ?, Xstop = ?, 
    Ystop = ?, StartTime = ?, StopTime = ?, Done = ?, Sequence = ?, CCD_overhead = ?
    WHERE ID = ?`, 
    [
      obdata.OBnumber, obdata.ins_date, obdata.Note, obdata._RA, obdata._DEC, obdata.Epoch, 
      obdata.Exp_type, obdata.Exp_time, obdata.Repeats, obdata.Binning, obdata.Guider, obdata.Filter, 
      obdata.Object, obdata.Dithering, obdata.Focus, obdata.Priority, obdata.Alt_limit, obdata.Status, 
      obdata.Activation, obdata.Xstart, obdata.Ystart, obdata.Xstop, obdata.Ystop, obdata.StartTime, 
      obdata.StopTime, obdata.Done, obdata.Sequence, obdata.CCD_overhead, id
    ]
  );

  let message = 'Error in updating obdata';

  if (result.affectedRows) {
    message = 'Obdata updated successfully';
  }

  return {message};
}

async function remove(id){
  const result = await db.query(
    `DELETE FROM obdata
    WHERE ID = ?`, 
    [id]
  )

  let message = 'No rows deleted';

  if(result.affectedRows)
  {
    message = result.affectedRows + " row(s) deleted"; 
  }

  return message
}


module.exports = {
  getMultiple, 
  getFromId, 
  getFromOBNumber,
  create, 
  update, 
  remove
}
