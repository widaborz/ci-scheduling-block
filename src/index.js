const express = require('express');
var cors = require('cors')
const bodyParser = require('body-parser');
const app = express();
const port = process.env.PORT || 3000;
const obdataRouter = require('./routes/obdata');
const oblistRouter = require('./routes/oblist')

var corsOptions = {
  origin: 'http://localhost:8080',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

app.use(cors(corsOptions)); 

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

app.get('/', cors(corsOptions), (req, res) => {
  res.json({'message': 'CI-Scheduling-Block API running!'});
})

app.use('/obdata', obdataRouter);
app.use('/oblist', oblistRouter); 

/* Error handler middleware */
app.use((err, req, res, next) => {
  const statusCode = err.statusCode || 500;
  console.error(err.message, err.stack);
  res.status(statusCode).json({'message': err.message});


  return;
});

app.listen(port, () => {
  console.log(`🚀 CI-Scheduling-Block API listening at http://localhost:${port}`)
});