const express = require('express');
const router = express.Router();
const obdata = require('../services/obdata');
const validator = require('validator');
const { nextTick } = require('process');

/* GET obdata by OBnumber */
router.get('/OBnumber/:OBnumber', async function(req, res, next) {
  try {
    if(!validator.isNumeric(req.params.OBnumber) || req.params.OBnumber == 0){
      res.send('Parameter error: invalid parameters OBnumber');
    } else{
      res.json(await obdata.getFromOBNumber(req.params.OBnumber));
    }
  } catch (err) {
    console.error(`Error while getting obdata `, err.message);
    next(err);
  }
});

/* GET obdata */
router.get('/', async function(req, res, next) {
  try {
    res.json(await obdata.getMultiple(req.query.page));
  } catch (err) {
    console.error(`Error while getting obdata `, err.message);
    next(err);
  }
});

/* GET obdata by ID */
router.get('/:id', async function(req, res, next) {
  try {
    if(!validator.isNumeric(req.params.id) || req.params.id == 0){
      res.send('Parameter error: invalid parameters');
    } else{
      res.json(await obdata.getFromId(req.params.id));
    }
  } catch (err) {
    console.error(`Error while getting obdata `, err.message);
    next(err);
  }
});

/* POST create obdata */
router.post('/', async function(req, res, next) {
  try {
    res.json(await obdata.create(req.body));
  } catch (err) {
    console.error(' Error while creating obdata ', err.message); 
    next(err); 
  }
}); 

/* PUT update obdata */
router.put('/:id', async function(req, res, next) {
  try {
    res.json(await obdata.update(req.params.id, req.body));
  } catch (err) {
    console.error(`Error while updating programming language`, err.message);
    next(err);
  }
});

/* REMOVE obdata */
router.delete('/:id', async function(req, res, next) {
  try {
    res.json(await obdata.remove(req.params.id)); 
  } catch (err) {
    console.error("Error while removing obdata ", err.message); 
    next(err); 
  }
}); 

module.exports = router;
