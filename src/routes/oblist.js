const express = require('express');
const router = express.Router();
const oblist = require('../services/oblist');
const validator = require('validator');

/* GET oblist */
router.get('/', async function(req, res, next) {
    console.log("get oblist")
    try {
      res.json(await oblist.getMultiple(req.query.page));
    } catch (err) {
      console.error(`Error while getting obdata `, err.message);
      next(err);
    }
  });

/* Get oblist form ID */
router.get('/:id', async function(req, res, next) {
    try {
        if(!validator.isNumeric(req.params.id) || req.params.id === 0)
            res.send('Error parsing ID');
        else
            res.json(await oblist.getByID(req.params.id)); 
    }
    catch (err) {
        console.error("Error while getting a oblist by ID ", err.message); 
        next(err); 
    }

});

/* POST new oblist */
router.post('/', async function(req, res, next) {
    try {
        res.json(await oblist.create(req.body)); 
    }
    catch (err) {
        console.log("Error while creating new oblist", err.message); 
        next(err); 
    }
}); 

/* PUT oblist */
router.put('/:id', async function(req, res, next) {
    try {
        res.json(await oblist.update(req.params.id, req.body)); 
    }
    catch (err) {
        console.error("Error while updating oblist", err.message); 
        next(err); 
    }
}); 

/* DELETE oblist */
router.delete('/:id', async function (req, res, next) {
    try {
        res.json(await oblist.remove(req.params.id)); 
    }
    catch (err) {
        console.error("Error while removing oblist ", err.message); 
        next(err)
    }
}); 

module.exports = router; 